from fastapi import APIRouter, HTTPException, status, Depends
from ..crud.base import CRUDBase
from ..schemas.products import ProductSchema
from ..models.products import FAKE_PRODUCTS
from ..schemas.stocks import StockSchema
from ..models.stocks import FAKE_STOCKS


router = APIRouter(
    prefix="/api/v1/products",
    tags=["products"],
    responses={404: {"description": "Not found"}}
)



@router.post(
    "",
    status_code=status.HTTP_201_CREATED,
)
async def create_product(product: ProductSchema):
    """
    Endpoint to create a new product
    """
    obj_crud = CRUDBase(FAKE_PRODUCTS)
    result = obj_crud.get(dynamic_id='sku', id_in=product.sku)
    if result:
        # if recorded exist
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="El registro ya existe."
        )


    fields = product.model_dump()
    register = obj_crud.create(obj_in=fields)
    if register:
        # if register was created then create a recorder with stock
        obj_crud_stock = CRUDBase(FAKE_STOCKS)
        fields_stock = StockSchema(product_id=register['id']).model_dump()
        register_stock = obj_crud_stock.create(obj_in=fields_stock)
    return {"detail": register}


@router.get("", status_code=status.HTTP_200_OK)
async def read_product(limit: int = 10, page: int = 1):
    """
    Endpoint to returning multiple data
    """
    obj_crud = CRUDBase(FAKE_PRODUCTS)
    skip = (page - 1) * limit
    result = obj_crud.get_multi(skip=skip, limit=limit)
    return {"detail": result}
