from fastapi import APIRouter, HTTPException, status, Depends, BackgroundTasks
from ..crud.base import CRUDBase
from ..schemas.orders import OrderSchema
from ..models.customers import FAKE_CUSTOMERS
from ..services.orders import Shopping
from ..models.orders import FAKE_ORDERS


router = APIRouter(
    prefix="/api/v1/orders",
    tags=["orders"],
    responses={404: {"description": "Not found"}}
)


@router.post(
    "",
    status_code=status.HTTP_201_CREATED,
)
async def create_order(order: OrderSchema, background_tasks: BackgroundTasks):
    """
    Endpoint to create order
    """
    # Valid if exist customer
    obj_crud = CRUDBase(FAKE_CUSTOMERS)
    result = obj_crud.get(dynamic_id='id', id_in=order.customer_id)
    if not result:
        # if recorded exist
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"El cliente {order.customer_id} no existe"
        )
    payload = order.model_dump()
    obj_shooping = Shopping(payload, FAKE_ORDERS, CRUDBase)
    response, check = obj_shooping.make_purchases(background_tasks)

    if check:
        response.update({"message": "Order placed successfully"})

    return response
