from fastapi import APIRouter, HTTPException, status, Depends
from ..crud.base import CRUDBase
#from ..schemas.products import ProductSchema
from ..models.customers import FAKE_CUSTOMERS


router = APIRouter(
    prefix="/api/v1/customers",
    tags=["customers"],
    responses={404: {"description": "Not found"}}
)


@router.get("", status_code=status.HTTP_200_OK)
async def read_customer(limit: int = 10, page: int = 1):
    """
    Endpoint to returning multiple data
    """
    obj_crud = CRUDBase(FAKE_CUSTOMERS)
    skip = (page - 1) * limit
    result = obj_crud.get_multi(skip=skip, limit=limit)
    return {"detail": result}
