from fastapi import APIRouter, HTTPException, status, Depends, BackgroundTasks
from ..crud.base import CRUDBase
from ..schemas.stocks import StockSchema, StockSchemaUpdate
from ..models.stocks import FAKE_STOCKS
from uuid import UUID
from ..services.stocks import write_notification


router = APIRouter(
    prefix="/api/v1/inventories",
    tags=["inventories"],
    responses={404: {"description": "Not found"}}
)


@router.get("", status_code=status.HTTP_200_OK)
async def read_stock(limit: int = 10, page: int = 1):
    """
    Endpoint to returning multiple data
    """
    obj_crud = CRUDBase(FAKE_STOCKS)
    skip = (page - 1) * limit
    result = obj_crud.get_multi(skip=skip, limit=limit)
    return {"detail": result}


@router.patch("/product/{id}", status_code=status.HTTP_200_OK)
async def update_stock(
    id: UUID,
    payload: StockSchemaUpdate,
    background_tasks: BackgroundTasks
):
    """
    Endpoint to update a recorded
    """
    obj_crud = CRUDBase(FAKE_STOCKS)
    result = obj_crud.get(dynamic_id='product_id', id_in=id)
    if not result:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Registro no encontrado con el id {id}"
        )
    obj_in = payload.model_dump(exclude_unset=True)
    update = obj_crud.update(
        recorded=result,
        obj_in=obj_in
    )
    if obj_in['quantity'] < 10:
        # write notification in console
        background_tasks.add_task(write_notification, product_id=id)
    return {"detail": update}
