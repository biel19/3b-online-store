# SPDX-FileCopyrightText: 2024-present aescobar19 <abe_ext@uniclick.mx>
#
# SPDX-License-Identifier: MIT
__version__ = '0.0.1'
