from pydantic import BaseModel
from uuid import UUID


class StockSchema(BaseModel):
    product_id: UUID
    quantity: int | None = 100


class StockSchemaUpdate(BaseModel):
    quantity: int
