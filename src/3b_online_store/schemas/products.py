from pydantic import BaseModel

class ProductSchema(BaseModel):
    name: str
    sku: str

