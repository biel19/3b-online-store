from pydantic import BaseModel
from uuid import UUID
from typing import List


class ItemBase(BaseModel):
    product_id: UUID
    quantity: int

class OrderSchema(BaseModel):
    customer_id: UUID
    items: List[ItemBase]

    class Config:
        from_attributes=True


