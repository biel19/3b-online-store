from ..models.products import FAKE_PRODUCTS
from ..models.stocks import FAKE_STOCKS
from ..schemas.stocks import StockSchema, StockSchemaUpdate


def write_notification(product_id=""):
    content = f"The product {product_id} stock is less than 10"
    print(content)


class Shopping:
    def __init__(self, order, model, instance):
        self.order = order
        self.model = model
        self.instance = instance
        self.stock = 10

    def make_purchases(self, background_tasks):
        """
        Method to make purchases
        """
        obj_product = self.instance(FAKE_PRODUCTS)
        obj_stock = self.instance(FAKE_STOCKS)
        message = []
        status = True
        for item in self.order['items']:
            # Loop to evalueate the purchases
            result_product = obj_product.get(dynamic_id='id', id_in=item["product_id"])
            if not result_product:
                # if the product record does not exist
                status = False
                message.append(
                    {
                        "detail": f"The product {item['product_id']} does not exist",
                        "status_code": 404,
                        "error": "Not Found"
                    }
                )
                break

            result_stock= obj_stock.get(dynamic_id='product_id', id_in=item["product_id"])
            if result_stock["quantity"] < item["quantity"]:
                # if the product stock is less than in payload
                status = False
                message.append(
                    {
                        "detail": f"The requested product {item['product_id']} is out of stock",
                        "status_code": 404,
                        "error": "Not Found"
                    }
                )
                break

            # Update product stock
            substrack_stock = result_stock["quantity"] - item["quantity"]
            if substrack_stock < self.stock:
                # write notification in console
                background_tasks.add_task(write_notification, product_id=item["product_id"])
            obj_in = StockSchemaUpdate(quantity=substrack_stock).model_dump(exclude_unset=True)
            update = obj_stock.update(
                recorded=result_stock,
                obj_in=obj_in
            )

        information_order = {
            "status": status,
            "message": "Failed to process the order",
            "details": message
        }

        return information_order, status

