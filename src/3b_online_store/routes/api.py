from fastapi import APIRouter
from ..endpoints import customers, products, stocks, orders

router = APIRouter()
router.include_router(customers.router)
router.include_router(products.router)
router.include_router(stocks.router)
router.include_router(orders.router)
