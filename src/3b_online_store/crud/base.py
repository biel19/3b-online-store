import json
from fastapi.encoders import jsonable_encoder
import uuid
from typing import Any


class CRUDBase():
    def __init__(self, model: Any = None):
        self.model = model

    def create(self, obj_in):
        """
        Method to create a new recorded
        """
        obj_in.update({"id": uuid.uuid4()})
        self.model.append(jsonable_encoder(obj_in))
        return obj_in

    def get_multi_all(self):
        """
        Method to get all records
        """
        return self.model

    def get_multi(self, skip: int = 0, limit: int = 100):
        """
        Method to get multiple recorded with pagination
        """
        return self.model[skip:limit]

    def get(self, dynamic_id, id_in):
        """
        Method to get a recorded
        """
        data = [element for element in self.model if element[dynamic_id] == str(id_in)]
        if data:
            return data[0]
        return []

    def update(self, recorded, obj_in):
        """
        Method to update a recorded
        """
        recorded.update(obj_in)
        return recorded
