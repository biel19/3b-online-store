FROM python:3.11.2 AS builder
ENV DEBIAN_FRONTEND=noninteractive \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100
RUN set -eux; \
    addgroup --gid 1000 tiendas3b; \
    adduser --system --uid 1000 --gid 1000 tiendas3b; \
    mkdir /opt/tiendas3b; \
    chown 1000:1000 -R /opt/tiendas3b
USER tiendas3b
WORKDIR /opt/tiendas3b
RUN set -eux; \
    python3 -m venv /opt/tiendas3b; \
    /opt/tiendas3b/bin/pip install --no-cache-dir --upgrade pip
RUN set -eux; \
    /opt/tiendas3b/bin/pip install hatch==1.7.0
COPY --chown=1000:1000 . .
RUN set -eux; \
    /opt/tiendas3b/bin/hatch build --clean

FROM python:3.11.2 AS stage
ENV DEBIAN_FRONTEND=noninteractive \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100
RUN set -eux; \
    addgroup --gid 1000 tiendas3b; \
    adduser --system --uid 1000 --gid 1000 tiendas3b; \
    mkdir /opt/tiendas3b; \
    chown 1000:1000 -R /opt/tiendas3b
RUN set -eux; \
    apt-get update; \
    apt-get upgrade -y; \
    apt-get install -y --no-install-recommends \
    libpq-dev \
    ; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/*
USER tiendas3b
WORKDIR /opt/tiendas3b
RUN set -eux; \
    python3 -m venv /opt/tiendas3b; \
    /opt/tiendas3b/bin/pip install --no-cache-dir --upgrade pip
COPY --from=builder --chown=1000:1000 /opt/tiendas3b/requirements.txt .
RUN set -eux; \
    /opt/tiendas3b/bin/pip install -r requirements.txt
COPY --from=builder --chown=1000:1000 /opt/tiendas3b/dist/ /opt/tiendas3b/dist/
RUN set -eux; \
    /opt/tiendas3b/bin/pip install --no-deps dist/*.whl; \
    find /opt/tiendas3b/lib -type d -name tests -exec rm -rf '{}' +; \
    find /opt/tiendas3b/lib -type d -name __pycache__ -exec rm -rf '{}' +; \
    rm -rf dist; \
    rm -rf requirements.txt
FROM python:3.11.2-slim AS deploy
ENV DEBIAN_FRONTEND=noninteractive \
    PYTHONFAULTHANDLER=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONHASHSEED=random \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100
RUN set -eux; \
    addgroup --gid 1000 tiendas3b; \
    adduser --system --uid 1000 --gid 1000 tiendas3b; \
    mkdir /opt/tiendas3b; \
    chown 1000:1000 -R /opt/tiendas3b
RUN set -eux; \
    apt-get update; \
    apt-get upgrade -y; \
    apt-get install -y --no-install-recommends \
    libpq-dev \
    ; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/*
USER tiendas3b
WORKDIR /opt/tiendas3b
COPY --from=stage /opt/tiendas3b /opt/tiendas3b
ENV PATH="/opt/tiendas3b/bin:${PATH}"
STOPSIGNAL SIGINT
EXPOSE 8000
CMD ["uvicorn", "3b_online_store.main:app", "--host=0.0.0.0", "--port=8000"]
