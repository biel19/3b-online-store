# Test:  Tiendas3b Online Store
You are developing a REST back end of an online store app.

## Previous requirements
1. Install docker

        https://drive.google.com/file/d/1KloNhZffMbVpa6hIuldiq5N4HvCjCbd8/view?usp=share_link

2. Install docker-compose

        sudo apt install docker-compose

## Installation
1. Clone the project

        https://gitlab.com/biel19/3b-online-store.git
2. Locate yourself on the root path of the project

        /path/3b-online-store
3. Build the image with docker

        docker build -t 3b-online-store:0.0.1 .
4. Pull up the image with docker-compose

        docker-compose up -d

## Validate project
You can validate service from:
* http://127.0.0.1:8000/docs

You can validate the job when the stock of a product becomes less than 10 from the console.
1. Execute command from the console

        docker ps -a

2. Identify the CONTAINER ID

        For example: 56d2ec7a32e8

3. Execute command from the console to print result in console

        docker logs [CONTAINER ID]
        For example: docker logs 56d2ec7a32e8

4. You can see the message from the console

!!! note

     Records are being saved in memory.
